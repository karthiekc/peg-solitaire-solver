package com.heuristics;

import com.State;
import com.util.PegSolitareUtil;

public class HeuristicRowColReduce implements IHeuristic{
	//State state;
	final int DIRECTION_LEFT = 1;
	final int DIRECTION_RIGHT = 2;
	final int boardSize = PegSolitareUtil.BOARD_SIZE;
	
	public HeuristicRowColReduce(){
	
	}
	
	/*
	public HeuristicRowColReduce(State s){
		this.state = s;
	}*/
	
	/*
	public int getHValue(){
		int minRowHValue = getMinRowHValue();
		int minColHValue = getMinColHValue();
		return Math.min(minRowHValue, minColHValue);
	}*/
	
	public int getHValue(State s){
		//this.state = s;
		int minRowHValue = getMinRowHValue(s);
		int minColHValue = getMinColHValue(s);
		return Math.min(minRowHValue, minColHValue);
	}
	
	private int getMinRowHValue(State s){
		int minRowHVal = Integer.MAX_VALUE;
		int[] rowCopy = new int[boardSize];
		int noOfPegsInRow, direction; 
		for(int row=0;row < boardSize;row++){
			if(!rowHasPeg(s, row))
				continue;
			
			copyRow(row, rowCopy, s);
					
			while((direction = moveExistsForRow(row, rowCopy))!=0){
				reducePegsInRow(direction, row, rowCopy);
			}
			noOfPegsInRow = getNumOfPegsInRow(row, s) - getNumOfPegsInRow(rowCopy);
			if(noOfPegsInRow < minRowHVal) {
				minRowHVal = noOfPegsInRow;
			}
		}
		return minRowHVal;
	}
	
	private int getNumOfPegsInRow(int row, State s){
		int count = 0;
		for(int j=0;j<boardSize;j++){
			if(s.board[row][j] == 1)
				count++;
		}
		return count;
	}
	
	private int getNumOfPegsInCol(int col, State s){
		int count = 0;
		for(int i=0;i<boardSize;i++){
			if(s.board[i][col] == 1)
				count++;
		}
		return count;
	}
	
	private boolean rowHasPeg(State s, int row){
		for(int j=0;j < boardSize;j++){
			if(s.board[row][j] == 1)
				return true;
		}
		return false;
	}
	
	private boolean colHasPeg(State s, int col){
		for(int i=0;i < boardSize;i++){
			if(s.board[i][col] == 1)
				return true;
		}
		return false;
	}
	
	private int getMinColHValue(State s){
		int minColHVal = Integer.MAX_VALUE;
		int[] colCopy = new int[boardSize];
		int noOfPegsInCol, direction; 
		for(int col=0;col < boardSize;col++){
			if(!colHasPeg(s, col))
				continue;
			
			copyCol(col, colCopy, s);
			
			//due to symmetry we can use moveExistsForRow to check for col also
			while((direction = moveExistsForRow(col, colCopy))!=0){
				//due to symmetry, we can use reducePegsInRow for column also
				reducePegsInRow(direction, col, colCopy);
			}
			noOfPegsInCol = getNumOfPegsInCol(col, s) - getNumOfPegsInRow(colCopy);
			if(noOfPegsInCol < minColHVal)
				minColHVal = noOfPegsInCol;
		}
		return minColHVal;
	}
	
	private int moveExistsForRow(int row, int[] rowCopy){
		int start = getFirstValidPositionForRow(row);
		int end = getLastValidPositionForRow(row);
		int j;
		
		//try left->right
		j = start;
		while(j <= end - 2 ){
			if(rowCopy[j] == 1 && rowCopy[j+1] == 1 && rowCopy[j+2] == 0)
				return DIRECTION_LEFT;
			j++;
		}
		
		j=end;
		while(j >= start+2){
			if(rowCopy[j] == 1 && rowCopy[j-1] == 1 && rowCopy[j-2] == 0)
				return DIRECTION_RIGHT;
			j--;
		}
		return 0;
	}
	
	private void copyRow(int row, int[] copy, State state){
		for(int j=0;j<boardSize;j++){
			copy[j] = state.board[row][j];
		}
	}
	
	private void copyCol(int col, int[] copy, State state){
		for(int i=0;i<boardSize;i++){
			copy[i] = state.board[i][col];
		}
	}
	
	private int getFirstValidPositionForRow(int row){
		if(row == 0 || row == 1 || row == 5 || row == 6)
			return 2;
		else
			return 0;
	}
	
	private int getLastValidPositionForRow(int row){
		if(row == 0 || row == 1 || row == 5 || row == 6)
			return 4;
		else
			return 6;
	}
	
	private void reducePegsInRow(int direction, int rowIndex, int[] rowCopy){
		int start = getFirstValidPositionForRow(rowIndex);
		int end = getLastValidPositionForRow(rowIndex);
		int j;
		
		if(direction == DIRECTION_LEFT){
			j = start;
			while(j <= end - 2 ){
				if(rowCopy[j] == 1 && rowCopy[j+1] == 1 && rowCopy[j+2] == 0){
					rowCopy[j] = 0;
					rowCopy[j+1] = 0;
					rowCopy[j+2] = 1;
					j++;
				}
				j++;
			}
		}
		else {
			j = end;
			while(j >= start+2){
				if(rowCopy[j] == 1 && rowCopy[j-1] == 1 && rowCopy[j-2] == 0){
					rowCopy[j] = 0;
					rowCopy[j-1] = 0;
					rowCopy[j-2] = 1;
					j--;
				}					
				j--;
			}
		}
	}
	
	private int getNumOfPegsInRow(int[] rowCopy){
		int count = 0;
		for(int i=0;i<boardSize;i++){
			if(rowCopy[i] == 1)
				count++;
		}
		return count;
	}
}
