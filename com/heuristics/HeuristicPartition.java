package com.heuristics;

import com.State;

public class HeuristicPartition implements IHeuristic{

	@Override
	public int getHValue(State s) {
		int verticalPartition,horizontalPartition;
		verticalPartition=horizontalPartition=0;
		
		for(int i=0;i<s.BOARD_SIZE;i++){
			int counter=0;
			boolean cont=false;
			for(int j=0;j<s.BOARD_SIZE;j++){
				if(s.board[i][j]==-1){
					continue;
				}
				else if(s.board[i][j]==1){
					if(counter<0) counter=0; //beginning of a partition
					cont=true;
					counter++;
					if(counter==3){
						counter=2;
						horizontalPartition++;
					}
				}
				else if(s.board[i][j]==0){
					counter--;
					if(counter==-1 && cont){
						horizontalPartition++;
						cont=false;
					}
				}
			}
			if(cont) horizontalPartition++;
		}
		for(int j=0;j<s.BOARD_SIZE;j++){
			int counter=0;
			boolean cont=false;
			for(int i=0;i<s.BOARD_SIZE;i++){
				if(s.board[i][j]==-1){
					continue;
				}
				else if(s.board[i][j]==1){
					if(counter<0) counter=0; //beginning of a partition
					cont=true;
					counter++;
					if(counter==3){
						counter=2;
						verticalPartition++;
					}
				}
				else if(s.board[i][j]==0){
					counter--;
					if(counter==-1 && cont){
						verticalPartition++;
						cont=false;
					}
				}
			}
			if(cont) verticalPartition++;
		}
		return Math.min(horizontalPartition,verticalPartition) - 1;
	}

}
