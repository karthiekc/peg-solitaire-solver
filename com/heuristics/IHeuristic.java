package com.heuristics;

import com.State;

public interface IHeuristic {
	int getHValue(State s);
}
