package com.heuristics;

import com.State;
import com.util.PegSolitareUtil;

public class HeuristicPegCount implements IHeuristic{

	final int boardSize = PegSolitareUtil.BOARD_SIZE;
	
	public int getHValue(State s){
		int count = 0;
		for(int i=0;i<boardSize;i++){
			for(int j=0;j<boardSize;j++){
				if(s.board[i][j] == 1)
					count++;
			}
		}
		return count - 1;
	}
}
