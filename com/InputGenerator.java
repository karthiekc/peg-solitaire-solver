package com;

import com.util.PegSolitareUtil;

public class InputGenerator {
	public static void main(String args[]){
	    int minNumOfPegs = 3, maxNumOfPegs = 22, count = 2;
	    String inputFileName = "randomInputs.txt";
	    PegSolitareUtil.generateRandomInputsToFile(minNumOfPegs, maxNumOfPegs, count, inputFileName);
	}
}
