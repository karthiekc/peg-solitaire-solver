package com;

import java.util.Comparator;

public class StateComparator implements Comparator<State>{

	@Override
	public int compare(State x, State y) {
		return x.FValue() - y.FValue();
	}

}
