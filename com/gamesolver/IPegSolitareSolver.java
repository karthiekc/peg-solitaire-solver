package com.gamesolver;

/*
* An interface for different classes that solve Peg solitare problem
*/

public interface IPegSolitareSolver {

	public boolean solvePegSolitare();
	
	public void printResult();
}
