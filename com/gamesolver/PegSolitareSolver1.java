package com.gamesolver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashSet;
import java.util.PriorityQueue;
import java.util.Set;

import com.State;
import com.StateComparator;
import com.util.PegSolitareUtil;

/*
* A peg solitare solver based on A* search without any pruning.
*/
public class PegSolitareSolver1 implements IPegSolitareSolver{

	private State startState, endState;
	
	//will contain set of all states to be expanded
	private PriorityQueue<State> openSet; 
	
	/*
	*will contain all states that are already expanded. Since it is just needed
	* for lookup, we use a hashtable
	*/
	private Hashtable<State, Object> closedSet = new Hashtable<State, Object>();

	/*
	* A hashtable to keep track of states visited from start state to goal state.
	*/
	private HashMap<State, State> cameFrom = new HashMap<State, State>();

	/*
	* A list to maintain the moves made to traverse from start to goal state.
	*/
	private ArrayList<String> pathToGoal = new ArrayList<String>();
	
	private long noOfIterations = 0;
	private boolean result; //goal state reached or not
	private long timeTaken = 0;
	
	public PegSolitareSolver1(State st){
		startState = st;
	}
	
	public PegSolitareSolver1(State st, PegSolitareUtil.HeuristicType heuristicType){
		startState = st;
		PegSolitareUtil.HeuristicForGame = heuristicType;
	}
	
	@Override
	public boolean solvePegSolitare() {
		startState.setGValue(0);
		openSet = new PriorityQueue<State>(10, new StateComparator());
		openSet.add(startState);
		
		long startTime = System.currentTimeMillis();
		
		while(!openSet.isEmpty()){
			noOfIterations++;
			State currState = openSet.remove();
			if(isGoalState(currState)){
				endState = currState;
				timeTaken = System.currentTimeMillis() - startTime;
				result = true;
				return true;
			}
			closedSet.put(currState, new Object());
			Set<State> neighborStates = getAllNextStates(currState);
			for(State neighbor:neighborStates){
				if(closedSet.get(neighbor)!=null) {
					//noOfClashes++;
					continue;
				}
							
				int tentative_g_score = currState.GValue() + 1;//1 = distance between currState and neighbor
				
				if(!openSet.contains(neighbor) || tentative_g_score < neighbor.GValue()){
					cameFrom.put(neighbor, currState);
					neighbor.setGValue(tentative_g_score);
					if(openSet.contains(neighbor)){
						//the neighbor in openSet has less more g_score.
						//remove it and add the one with lesser g_score.
						openSet.remove(neighbor);
					}
					//else //new state
					//	noOfStates++;
					
					openSet.add(neighbor);
				}
			}
			
		}
		
		timeTaken = System.currentTimeMillis() - startTime;
		result = false;
		return result;
	}
	
	public void reconstructPath(State currState){
		State parentState = cameFrom.get(currState);
		if(parentState != null){
			reconstructPath(parentState);
			pathToGoal.add(currState.move);
		}
	}

	@Override
	public void printResult() {
		//noOfStates = closedSet.size();
		//System.out.println("Printing Statistics:\n");
		if(result) {
			System.out.println("Result: Reached goal!");
			reconstructPath(endState);
			if(pathToGoal.size() > 0){
				System.out.print("Moves: <");
				int i;
				for(i=0; i< pathToGoal.size()-1; i++)
					System.out.print(pathToGoal.get(i) + ", ");
				System.out.println(pathToGoal.get(i) + ">");
			}
		}
		else
			System.out.println("Result: Cannot reach goal!");
		
		System.out.println("Running time (in milliseconds): " + timeTaken/1000.0);
		System.out.println("States unexpanded: " + openSet.size());
		System.out.println("States generated: " + noOfIterations);
		//System.out.println("No. of clashes: " + noOfClashes);
		System.out.println("States expanded: " + closedSet.size());
	}
	
	private boolean isGoalState(State state){
		for(int i=0;i<state.BOARD_SIZE;i++){
			for(int j=0;j<state.BOARD_SIZE;j++){
				if(i == (state.BOARD_SIZE/2) && j == (state.BOARD_SIZE/2)){
					if(state.board[i][j] != 1)
						return false;
				}
				else if(state.board[i][j] == 1)
					return false;
			}
		}
		return true;
	}
	
	private LinkedHashSet<State> getAllNextStates(State currState){
		LinkedHashSet<State> allNextStates = new LinkedHashSet<State>();
		int boardSize = currState.BOARD_SIZE;
		
		for (int i=0;i<boardSize;i++){
			for(int j=0;j<boardSize;j++){
				ArrayList<State> nextStates = getNextStates(currState, i, j);
				for(State s:nextStates){
					allNextStates.add(s);
				}
			}
		}
		
		return allNextStates;
	}
	
	
	/**
	 * For a given board state and a position in the board,
	 * generate possible adjacent states(maximum 4) for that position
	 * @param currState
	 * @param i
	 * @param j
	 * @return
	 */
	private ArrayList<State> getNextStates(State currState, int i, int j){
		ArrayList<State> nextStates = new ArrayList<State>();
		int boardSize = currState.BOARD_SIZE;
		
		if(isValidPosition(i,j, boardSize) && currState.board[i][j] == 1){
			State newState;

			//check left
			if(isValidPosition(i, j-1, boardSize) && isValidPosition(i, j-2, boardSize)
					&& currState.board[i][j-1] == 1 && currState.board[i][j-2] == 0){
				newState = currState.getCopy();
				newState.board[i][j] = 0;newState.board[i][j-1] = 0;newState.board[i][j-2] = 1;
				newState.move = "(" + PegSolitareUtil.validMovesIndex[i][j] + "," 
						+ PegSolitareUtil.validMovesIndex[i][j-2] + ")";
				nextStates.add(newState);
			}
			//check right
			if(isValidPosition(i, j+1, boardSize) && isValidPosition(i, j+2, boardSize)
					&& currState.board[i][j+1] == 1 && currState.board[i][j+2] == 0){
				newState = currState.getCopy();
				newState.board[i][j] = 0;newState.board[i][j+1] = 0;newState.board[i][j+2] = 1;
				newState.move = "(" + PegSolitareUtil.validMovesIndex[i][j] + "," 
						+ PegSolitareUtil.validMovesIndex[i][j+2] + ")";
				nextStates.add(newState);
			}
			//check up
			if(isValidPosition(i-1,j,boardSize) && isValidPosition(i-2, j, boardSize)
					&& currState.board[i-1][j] ==1 && currState.board[i-2][j] == 0){
				newState = currState.getCopy();
				newState.board[i][j] = 0;newState.board[i-1][j] = 0;newState.board[i-2][j] = 1;
				newState.move = "(" + PegSolitareUtil.validMovesIndex[i][j] + "," 
						+ PegSolitareUtil.validMovesIndex[i-2][j] + ")";
				nextStates.add(newState);
			}
			//check down
			if(isValidPosition(i+1, j, boardSize) && isValidPosition(i+2, j, boardSize)
					&& currState.board[i+1][j] == 1 && currState.board[i+2][j] == 0){
				newState = currState.getCopy();
				newState.board[i][j] = 0;newState.board[i+1][j] = 0;newState.board[i+2][j] = 1;
				newState.move = "(" + PegSolitareUtil.validMovesIndex[i][j] + "," 
						+ PegSolitareUtil.validMovesIndex[i+2][j] + ")";
				nextStates.add(newState);
			}
		}	
		
		return nextStates;
	}
	
	private boolean isValidPosition(int i, int j, int size){
		return PegSolitareUtil.isValidPosition(i, j, size);
	}
}
