package com.gamesolver;

import java.util.ArrayList;
import java.util.Hashtable;

import com.State;
import com.util.PegSolitareUtil;


/**
* Class to solve Peg Solitare using Depth-first search
*/
public class DFSPegSolitareSolver implements IPegSolitareSolver {
	private State startState, currState;
	private Hashtable<State, Object> closedSet = new Hashtable<State, Object>();
	private long noOfIterations = 0;
	private int boardSize;
	private long timeTaken;
	private boolean result;
	private ArrayList<String> moves = new ArrayList<String>();
	
	public DFSPegSolitareSolver(State s) {
		startState = s;
		boardSize = s.BOARD_SIZE;
	}

	public boolean DFS(State s) {
		if (closedSet.get(s) != null)
			return false;

		closedSet.put(s, new Object());
		noOfIterations++;
		long count = 0;//keeps track of number of pegs in board

		for (int x = 0; x < boardSize; x++) {
			for (int y = 0; y < boardSize; y++) {
				int xhop1 = 0, xhop2 = 0, yhop1 = 0, yhop2 = 0;
				if (s.board[x][y] == 1) { 
					count++;
					
					//check for a possible move from given state
					for (int i = -1; i <= 1; i++) {
						for (int j = -1; j <= 1; j++) {
							boolean valid_move = false;
							if (i != 0
									&& j == 0
									&& PegSolitareUtil.isValidPosition(x + i,y, boardSize)
									&& PegSolitareUtil.isValidPosition(x + i + i, y, boardSize)
									&& s.board[x + i][y] == 1
									&& s.board[x + i + i][y] == 0) {
								xhop1 = x + i;
								xhop2 = x + i + i;
								yhop1 = yhop2 = y;
								valid_move = true;
							} else if (i == 0
									&& j != 0
									&& PegSolitareUtil.isValidPosition(x,y + j, boardSize)
									&& PegSolitareUtil.isValidPosition(x, y + j	+ j, boardSize)
									&& s.board[x][y + j] == 1
									&& s.board[x][y + j + j] == 0) {
								xhop1 = xhop2 = x;
								yhop1 = y + j;
								yhop2 = y + j + j;
								valid_move = true;
							}
							if (valid_move) {
								State newState = s.getCopy();
								newState.board[xhop2][yhop2] = 1;
								newState.board[xhop1][yhop1] = newState.board[x][y] = 0;
								
								//do DFS for new state
								if (DFS(newState)) {
									int fromIndex = PegSolitareUtil.validMovesIndex[x][y];
									int toIndex = PegSolitareUtil.validMovesIndex[xhop2][yhop2];
									moves.add("(" + fromIndex + "," + toIndex + ")");
									return true;
								}
							}
						}
					}
				}
			}
		}
		return (count == 1 && s.board[3][3] == 1);
	}

	@Override
	public boolean solvePegSolitare() {
		long startTime = System.currentTimeMillis();
		result = DFS(startState);
		long endTime = System.currentTimeMillis();
		timeTaken = endTime - startTime;
		return result;
	}

	@Override
	public void printResult() {
		if(result) {
			System.out.println("Result: Reached goal");
			if(moves.size() > 0)
			{
				System.out.print("Moves: ");
				System.out.print("<");
				for(int i=moves.size()-1;i>0;i--)
					System.out.print(moves.get(i) + ",");
				
				System.out.println(moves.get(0) + ">");
			}
		}
		else
			System.out.println("Result: Cannot reach goal");
		System.out.println("Running time (in seconds): " + timeTaken/1000.0);
		System.out.println("States expanded: " + closedSet.size());
	}

}
