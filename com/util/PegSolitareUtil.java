package com.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.State;
import com.heuristics.*;

/**
* A utility class containing common functions and constants.
*/
public class PegSolitareUtil {
	
	public static final int BOARD_SIZE = 7;
	
	public static int[][] validMovesIndex = {
			{0,0,0,1,2,0,0},
			{0,0,3,4,5,0,0},
			{6,7,8,9,10,11,12},
			{13,14,15,16,17,18,19},
			{20,21,22,23,24,25,26},
			{0,0,27,28,29,0,0},
			{0,0,30,31,32,0,0}
	};
	
	public static enum HeuristicType {
		ROW_COL_REDUCE, PEG_COUNT, PARTITION_COUNT
	}
	
	//not set as "final". Will be set at run time.
	public static HeuristicType HeuristicForGame = HeuristicType.ROW_COL_REDUCE;
		
	public static State getBoardState(String input){
		input = input.replace(" ", "");
		input = input.substring(1, input.length() - 1);
		String[] rows = input.split(",");
		return getBoardState(rows);
	}
	/**
	 * Return board state corresponding to given configuration
	 * @param strState - char array of size 33
	 * @return
	 */
	public static State getBoardState(String[] rows){
		State state = new State();
		//int arrIndex = 0;
		String row;
		for(int i=0;i<BOARD_SIZE;i++){
			row = rows[i];
			for(int j=0;j<BOARD_SIZE;j++){
				if(rows[i].charAt(j) != '-'){
					state.board[i][j] = rows[i].charAt(j) == '0'?0:1;
				}
				/*if(isValidPosition(i, j, BOARD_SIZE)){
					state.board[i][j] = strState[arrIndex++] == '0'?0:1;
				}*/
			}
		}
		return state;
	}
	
	public static void fillRandomBoardState(int[][] board, int numOfPegs){
		int i,j;
		boolean filled;
		for(int n=0;n < numOfPegs;n++){
			filled = false;
			do{
				i = (int)(Math.random()*BOARD_SIZE);
				j = (int)(Math.random()*BOARD_SIZE);
				if(!isValidPosition(i, j, BOARD_SIZE) || board[i][j] == 1)
					continue;
				board[i][j] = 1;
				//System.out.println("set board[" + i + "][" + j + "]");
				filled = true;
			}while(!filled);
		}
	}
	
	/**
	* Checks if given position on the board is valid
	*/
	public static boolean isValidPosition(int i, int j, int size){
		return (i >= 0 && j >= 0 && i < size && j < size) && 
				!( (i < 2 && j < 2) || (i < 2 && j > 4) 
						|| (i > 4 && j < 2) || (i > 4 && j > 4) 
				  );
	}
	
	/*
	* A factory function to generate heuristic
	* class based on type.
	*/
	public static IHeuristic getHeuristic(){
		switch(HeuristicForGame){
			case ROW_COL_REDUCE:
				return new HeuristicRowColReduce();
			case PEG_COUNT:
				return new HeuristicPegCount();
			case PARTITION_COUNT:
				return new HeuristicPartition();
			default:
				return null;
		}
	}
	
	public static String generateRandomInput(int numOfPegs){
		int[][] board = new int[BOARD_SIZE][BOARD_SIZE];
		boolean filled;
		int i,j;
		for(int n=0;n < numOfPegs;n++){
			filled = false;
			do{
				i = (int)(Math.random()*BOARD_SIZE);
				j = (int)(Math.random()*BOARD_SIZE);
				if(!isValidPosition(i, j, BOARD_SIZE) || board[i][j] == 1)
					continue;
				board[i][j] = 1;
				//System.out.println("set board[" + i + "][" + j + "]");
				filled = true;
			}while(!filled);
		}
		return getString(board);
	}
	
	/*
	 * Generate random board configuration 
	 * based on number of pegs.
	 * 
	 */
	public static String[] generateRandomInputs(int minNumOfPegs, int maxNumOfPegs, int count){
		String[] input = new String[count*(maxNumOfPegs - minNumOfPegs + 1)];
		int c, index = 0;
		for(int noOfPegs=minNumOfPegs;noOfPegs <= maxNumOfPegs;noOfPegs++) {
			for(c=0;c<count;c++){
				input[index++] = generateRandomInput(noOfPegs);
			}
		}
		return input;
	}
	
	public static String getString(int[][] board){
		StringBuffer str = new StringBuffer();
		str.append("<");
		String invalid = "-";
		String empty = "0";
		String occupied = "X";
		for(int i=0;i<BOARD_SIZE;i++){
			for(int j=0;j<BOARD_SIZE;j++){
				if(!isValidPosition(i, j, BOARD_SIZE))
					str.append(invalid);
				else if(board[i][j] == 1)
					str.append(occupied);
				else
					str.append(empty);
			}
			if(i != BOARD_SIZE - 1)
				str.append(",");
		}
		str.append(">");
		return str.toString();
	}
	
	public static void generateRandomInputsToFile(int minNumOfPegs, int maxNumOfPegs, int count, String fileName){
		try {
			FileOutputStream fs = new FileOutputStream(new File(fileName));
			String[] inputs = generateRandomInputs(minNumOfPegs, maxNumOfPegs, count);
			for(String s:inputs){
				fs.write(s.getBytes());
				fs.write("\n".getBytes());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
