package com.finaltests;

import java.io.File;
import java.io.FileInputStream;
import java.io.PrintStream;
import java.util.Scanner;

import com.State;
import com.gamesolver.IPegSolitareSolver;
import com.gamesolver.PegSolitareSolverWithPagoda3;
import com.util.PegSolitareUtil;

public class TestAStarWithHeuristic2WithPruning {
	public static void main(String args[]){
	    Scanner scanner = new Scanner(System.in);
  	    if(args.length == 0){
	    	System.out.println("Need an input file");
	    	return;
	    }
	    
	    //String inputFileName = "randomInputs.txt";
	    String inputFileName = args[0];

	    String outputFileName = "output-heuristic2-with-pruning.txt";
	    PrintStream console = System.out;
	    long startTime = System.currentTimeMillis();
	    
	    try {
			scanner = new Scanner(new FileInputStream(new File(inputFileName)));
			System.setOut(new PrintStream(new File(outputFileName)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	    PegSolitareUtil.HeuristicForGame = PegSolitareUtil.HeuristicType.PEG_COUNT;
	    
	    int caseId = 1;
	    try 
	    {
		    while (scanner.hasNextLine()) {
			    String input = scanner.nextLine();
				State startState = PegSolitareUtil.getBoardState(input);
	
				System.out.println("Case " + caseId + ": ");
				console.println("Running Case " + caseId++ + ": ");
				System.out.println("Input: " + input);
				console.println("Input: "+ input);
				System.out.println("Start state:");
				startState.printBoard();
				
				IPegSolitareSolver solver = new PegSolitareSolverWithPagoda3(startState);
				solver.solvePegSolitare();
				solver.printResult();
				
				System.out.println();
		    }
		    console.println("Done!!");
		    long endTime = System.currentTimeMillis();
		    console.println("Total time taken: " + (endTime - startTime));
		    System.out.println("Total time taken: " + (endTime - startTime));
		    
    	}catch(OutOfMemoryError err){
    		console.println("Ran out of memory. Program terminated.");
    	}
	    
	}
}
