package com;


import java.util.Arrays;
import com.heuristics.*;
import com.util.PegSolitareUtil;
/*
 * Class representing state of board in Peg Solitare
 */
public class State {
	public final int BOARD_SIZE = 7;
	public int[][] board;
	public String move;
	
	private int f_score = -1;
	private int g_score = Integer.MAX_VALUE;
	private int h_score = -1; //initially not set
	
	private static IHeuristic heuristic;
	
	
	public State(){
		board = new int[BOARD_SIZE][BOARD_SIZE];
		initBoard();
		heuristic = PegSolitareUtil.getHeuristic();
	}
	
	public void setHeuristicFunction(IHeuristic h){
		heuristic = h;
	}
	
	public void initBoard(){
		setInvalidSquares(0, 0);
		setInvalidSquares(0, 5);
		setInvalidSquares(5, 0);
		setInvalidSquares(5, 5);
	}
	
	public void setToRandomConfig(int numOfPegs){
		PegSolitareUtil.fillRandomBoardState(board, numOfPegs);
	}
	
	public void printBoard(){
		for(int i=0;i<BOARD_SIZE;i++){
			for(int j=0;j<BOARD_SIZE;j++){
				if(board[i][j] == -1)
					System.out.print("-");
				else if(board[i][j] == 0)
					System.out.print("0");
				else
					System.out.print("X");
			}
			System.out.println();
		}
	}
	
	public int HValue(){
		if(h_score == -1){
			h_score = heuristic.getHValue(this);
		}
		return h_score;
	}
	
	public int FValue(){
		if(f_score == -1){
			f_score = g_score + this.HValue();
		}
		return f_score;
	}
	
	public int GValue(){
		return g_score;
	}
	
	public void setGValue(int val){
		g_score = val;
	}
	
	/*
	 * Just copies board configuration.
	 * Does not set g_score or f_score
	 */
	public State getCopy(){
		State s = new State();
		//s.g_score = this.g_score;
		//s.h_score = this.h_score;
		//s.f_score = this.f_score;
		
		for(int i=0;i<BOARD_SIZE;i++){
			for(int j=0;j<BOARD_SIZE;j++){
				s.board[i][j] = this.board[i][j];
			}
		}
		
		return s;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.deepHashCode(board);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof State)) {
			return false;
		}
		State other = (State) obj;
		if (!Arrays.deepEquals(board, other.board)) {
			return false;
		}
		/*
		for(int i=0;i<BOARD_SIZE;i++){
			for(int j=0;j<BOARD_SIZE;j++){
				if(board[i][j] != other.board[i][j])
					return false;
			}
		}*/
		return true;
	}
	
	private void setInvalidSquares(int leftTop_i, int leftTop_j ){
		for(int i=leftTop_i;i<leftTop_i+2;i++)
			for(int j=leftTop_j;j<leftTop_j+2;j++)
				board[i][j] = -1;
	}		
}
